// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "RightPlayerPawn.h"
#include "PaperSpriteComponent.h"
#include "PongGameMode.h"
#include "PongGameInstance.h"
#include "LeftPlayerPawn.h"



// Sets default values
ARightPlayerPawn::ARightPlayerPawn() {
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UPaperSprite> player_pawn_sprite(TEXT("PaperSprite'/Game/right_player.right_player'"));

	PlayerPaperSprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("RightSprite"));
	PlayerPaperSprite->AttachTo(Root);
	PlayerPaperSprite->SetSprite(player_pawn_sprite.Object);
	PlayerPaperSprite->SetAbsolute(true, true, true);
	PlayerPaperSprite->SetWorldLocation(FVector(0, 470, 0));
	PlayerPaperSprite->SetWorldRotation(FRotator(0, 90, 90));
	PlayerPaperSprite->SetIsReplicated(true);
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	SetReplicates(true);
	SetReplicateMovement(true);


}

// Called when the game starts or when spawned
void ARightPlayerPawn::BeginPlay() {
	Super::BeginPlay();

}

// Called every frame
void ARightPlayerPawn::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ARightPlayerPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent) {
	Super::SetupPlayerInputComponent(InputComponent);
	InputComponent->BindAxis(FName("RightMove"), this, &ARightPlayerPawn::PlayerMove);
	if (Cast<UPongGameInstance>(GetGameInstance())->mode == 0)
		InputComponent->BindAxis(FName("LeftMove"), this, &ARightPlayerPawn::LeftPlayerMove);

}

void ARightPlayerPawn::PlayerMove(float a) {
	if (a != 0)
		PlayerPaperSprite->AddWorldOffset(FVector(a, 0, 0)*GetWorld()->GetDeltaSeconds() * 300, true);
}

void ARightPlayerPawn::LeftPlayerMove(float a) {
	if (a != 0)
		Cast<APongGameMode>(UGameplayStatics::GetGameMode(GetWorld()))->LeftPawn->PlayerMove(a);
}

