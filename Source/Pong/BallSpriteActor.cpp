// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "BallSpriteActor.h"
#include "GoalZoneActor.h"
#include "BasePlayerPawn.h"
#include "ReflectZoneActor.h"
#include "PongGameState.h"
#include "PongGameInstance.h"

ABallSpriteActor::ABallSpriteActor() {
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UPaperSprite> sprite(TEXT("PaperSprite'/Game/ball_Sprite.ball_Sprite'"));
	GetRenderComponent()->SetSprite(sprite.Object);
	RootComponent = GetRenderComponent();
	RootComponent->SetMobility(EComponentMobility::Movable);
	SetActorRotation(FRotator(0, 0, 90));
	SetActorLocation(FVector(0, 0, 0));
	SetActorScale3D(FVector(0.25, 0.25, 0.25));
	SetReplicates(true);
	SetReplicateMovement(true);

	ReplicatedMovement.LocationQuantizationLevel = EVectorQuantization::RoundTwoDecimals;
	ReplicatedMovement.VelocityQuantizationLevel = EVectorQuantization::RoundTwoDecimals;

}

void ABallSpriteActor::BeginPlay() {
	Super::BeginPlay();
	BallSpeed = Cast<UPongGameInstance>(GetGameInstance())->BallSpeed;

	Cast<APongGameState>(UGameplayStatics::GetGameState(GetWorld()))->Ball = this;
	InitBall();
}

void ABallSpriteActor::Tick(float delta) {
	Super::Tick(delta);
	Move();
}

void ABallSpriteActor::InitBall() {
	bPostWin = Cast<APongGameState>(UGameplayStatics::GetGameState(GetWorld()))->bPostWin;
	SetActorLocation(FVector(0, 0, 0));
	FMath::SRandInit(FDateTime::Now().GetTimeOfDay().GetMilliseconds());
	MoveDir = FVector2D(FVector(-10.f + FMath::SRand() * 20, -10.f + FMath::SRand() * 20, 0).GetClampedToSize2D(10, 10))*BallSpeed;
}

void ABallSpriteActor::Move() {
	if (bPostWin)return;
	FHitResult hit;
	AddActorWorldOffset(FVector(MoveDir, 0)*GetWorld()->GetDeltaSeconds() * 50, true, &hit);

	if (!hit.IsValidBlockingHit())return;

	if (Cast<AReflectZoneActor>(hit.GetActor()) != nullptr) {
		OnReflectZoneHit(hit);
	} else if (Cast<ABasePlayerPawn>(hit.GetActor()) != nullptr) {
		OnPingPongHit(hit);
	} else if (Cast<AGoalZoneActor>(hit.GetActor()) != nullptr) {
		OnGoalZoneHit(hit);
	}
}

void ABallSpriteActor::OnReflectZoneHit(FHitResult& hit) { MoveDir = FVector2D(FVector(MoveDir, 0).MirrorByVector(hit.Normal)); }

void ABallSpriteActor::OnPingPongHit(FHitResult& hit) {
	MoveDir = FVector2D((GetActorLocation() - hit.GetComponent()->GetComponentLocation()).GetClampedToMaxSize(10))*BallSpeed;
}

void ABallSpriteActor::OnGoalZoneHit(FHitResult& hit) {
	AGoalZoneActor* GoalZone = Cast<AGoalZoneActor>(hit.GetActor());
	if (Role == ROLE_Authority)
		Cast<APongGameState>(UGameplayStatics::GetGameState(GetWorld()))->ServerAddScore(GoalZone->player == 0 ? 1 : 0, 1);
	InitBall();
}