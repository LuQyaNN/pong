// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "PongGameInstance.generated.h"



/**
 * 
 */
UCLASS()
class PONG_API UPongGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	
	UPROPERTY(BlueprintReadWrite)
		float BallSpeed = 1;

	UPROPERTY(BlueprintReadWrite)
		int32 mode;

	UPROPERTY(BlueprintReadWrite)
		int32 GoalsForWin = 5;
};
