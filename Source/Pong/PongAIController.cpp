// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "PongAIController.h"
#include "PongGameState.h"
#include "BasePlayerPawn.h"
#include "BallSpriteActor.h"
#include "PongGameInstance.h"

APongAIController::APongAIController() :mode(-1) {
	PrimaryActorTick.bCanEverTick = true;

}

void APongAIController::BeginPlay() {
	Super::BeginPlay();
	mode = Cast<UPongGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()))->mode;
}

void APongAIController::Tick(float delta) {
	Super::Tick(delta);
	if (mode == 0)return;

	APongGameState* state = Cast<APongGameState>(UGameplayStatics::GetGameState(GetWorld()));

	if (state->Ball.IsValid()) {
		FVector ball_x = state->Ball->GetActorLocation();

		ABasePlayerPawn * our = Cast<ABasePlayerPawn>(GetPawn());
		if (our == nullptr)return;

		FVector our_x = our->PlayerPaperSprite->GetComponentLocation();
		if (ball_x != our_x && FVector::Dist(ball_x, our_x) < 500) {

			our->PlayerMove((ball_x - our_x).X > 0 ? 1 : -1);
		}

	}
}


