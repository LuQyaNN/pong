// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "LeftPlayerPawn.h"
#include "PaperSpriteComponent.h"


// Sets default values
ALeftPlayerPawn::ALeftPlayerPawn() {
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UPaperSprite> player_pawn_sprite(TEXT("PaperSprite'/Game/right_player.right_player'"));



	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	PlayerPaperSprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("LeftSprite"));
	PlayerPaperSprite->AttachTo(Root);
	PlayerPaperSprite->SetSprite(player_pawn_sprite.Object);
	PlayerPaperSprite->SetAbsolute(true, true, true);
	PlayerPaperSprite->SetWorldLocation(FVector(0, -470, 0));
	PlayerPaperSprite->SetWorldRotation(FRotator(0, -90, 90));
	PlayerPaperSprite->SetIsReplicated(true);

	SetReplicates(true);
	SetReplicateMovement(true);

}

// Called when the game starts or when spawned
void ALeftPlayerPawn::BeginPlay() {
	Super::BeginPlay();

}

// Called every frame
void ALeftPlayerPawn::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ALeftPlayerPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent) {
	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAxis(FName("LeftMove"), this, &ALeftPlayerPawn::PlayerMove);
}


void ALeftPlayerPawn::PlayerMove(float a) {
	if (a != 0)
		PlayerPaperSprite->AddWorldOffset(FVector(a, 0, 0)*GetWorld()->GetDeltaSeconds() * 300, true);
}