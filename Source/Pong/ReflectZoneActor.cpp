// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "ReflectZoneActor.h"


// Sets default values
AReflectZoneActor::AReflectZoneActor() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("collision"));
	RootComponent = Box;
	Box->SetBoxExtent(FVector(20, 1000, 20));
	Box->SetCollisionProfileName(FName("BlockAllDynamic"));
}

// Called when the game starts or when spawned
void AReflectZoneActor::BeginPlay() {
	Super::BeginPlay();

}

// Called every frame
void AReflectZoneActor::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

