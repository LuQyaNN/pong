// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "PongGameState.h"
#include "UnrealNetwork.h"
#include "PongGameInstance.h"

APongGameState::APongGameState() { players_scores.SetNum(2); }


void APongGameState::ServerAddScore_Implementation(int32 player, int32 score) {
	players_scores[player] += score;

	OnScoreChanged.Broadcast(player, players_scores[player]);
	if (Cast<UPongGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()))->GoalsForWin == players_scores[player]) {
		WinPlayer = player + 1;
		OnWin.Broadcast(WinPlayer);
		bPostWin = true;
		GetWorld()->GetTimerManager().SetTimer(WinTimer, this, &APongGameState::OnWinTimer, 5);
	}
}

void APongGameState::OnWinTimer() {
	UGameplayStatics::OpenLevel(GetWorld(), FName("Menu"));
}

void APongGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty > & OutLifetimeProps)const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APongGameState, players_scores);
	DOREPLIFETIME(APongGameState, bPostWin);
}