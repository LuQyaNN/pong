// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "PongGameMode.generated.h"

/**
 * 
 */
UCLASS()
class PONG_API APongGameMode : public AGameMode
{
	GENERATED_BODY()
public:
		APongGameMode();
	
		virtual void PostLogin(APlayerController* NewPlayer) override;

		class ALeftPlayerPawn* LeftPawn;
		class ARightPlayerPawn* RightPawn;
		
private:
	int32 mode;
};
