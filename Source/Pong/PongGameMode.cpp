// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "PongGameMode.h"
#include "RightPlayerPawn.h"
#include "PongGameState.h"
#include "PongAIController.h"
#include "LeftPlayerPawn.h"
#include "RightPlayerPawn.h"
#include "PongGameInstance.h"


APongGameMode::APongGameMode() {
	GameStateClass = APongGameState::StaticClass();

}

void APongGameMode::PostLogin(APlayerController* NewPlayer) {
	Super::PostLogin(NewPlayer);
	mode = Cast<UPongGameInstance>(GetGameInstance())->mode;
	switch (mode) {
	case 0: {
		RightPawn = Cast<ARightPlayerPawn>(GetWorld()->SpawnActor(ARightPlayerPawn::StaticClass()));
		NewPlayer->Possess(RightPawn);
		LeftPawn = Cast<ALeftPlayerPawn>(GetWorld()->SpawnActor(ALeftPlayerPawn::StaticClass()));
		break; }
	case 1: {
		RightPawn = Cast<ARightPlayerPawn>(GetWorld()->SpawnActor(ARightPlayerPawn::StaticClass()));
		NewPlayer->Possess(RightPawn);
		LeftPawn = Cast<ALeftPlayerPawn>(GetWorld()->SpawnActor(ALeftPlayerPawn::StaticClass()));
		break;}
	}

	/*
	if (GameState->PlayerArray.Num() == 2) {
	ALeftPlayerPawn* a = Cast<ALeftPlayerPawn>(GetWorld()->SpawnActor(ALeftPlayerPawn::StaticClass()));
	NewPlayer->Possess(a);
	}*/
}


