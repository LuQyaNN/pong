// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "GoalZoneActor.h"
#include "PongGameState.h"


// Sets default values
AGoalZoneActor::AGoalZoneActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("collision"));
	RootComponent = Box;
	Box->SetBoxExtent(FVector(1000, 20, 20));
	Box->SetCollisionProfileName(FName("BlockAllDynamic"));
}

// Called when the game starts or when spawned
void AGoalZoneActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGoalZoneActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AGoalZoneActor::NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
}