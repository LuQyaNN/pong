// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperSpriteActor.h"
#include "BallSpriteActor.generated.h"

/**
 * 
 */
UCLASS()
class PONG_API ABallSpriteActor : public APaperSpriteActor
{
	GENERATED_BODY()
public:
	ABallSpriteActor();
	void BeginPlay() override;
	void Move();
	void InitBall();

	void Tick(float DeltaSeconds) override;

	FVector2D MoveDir;
	void OnReflectZoneHit(FHitResult& hit);
	void OnPingPongHit(FHitResult& hit);
	void OnGoalZoneHit(FHitResult& hit);
private:
	bool bPostWin;
	float BallSpeed;
};
