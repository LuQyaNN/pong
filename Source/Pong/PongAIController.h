// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "PongAIController.generated.h"

/**
 * 
 */
UCLASS()
class PONG_API APongAIController : public AAIController
{
	GENERATED_BODY()
public:
	APongAIController();
	void Tick(float delta) override;
	void BeginPlay() override;
private:
	int32 mode;
	
	
};
