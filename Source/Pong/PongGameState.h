// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameState.h"
#include "BallSpriteActor.h"
#include "PongGameState.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnScoreChanged, int32, player, int32, score);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWin, int32, player);
/**
 * 
 */
UCLASS()
class PONG_API APongGameState : public AGameState
{
	GENERATED_BODY()

public:
	APongGameState();

	TWeakObjectPtr<ABallSpriteActor> Ball;
	
	UPROPERTY(BlueprintAssignable)
	FOnScoreChanged OnScoreChanged;

	UPROPERTY(BlueprintAssignable)
	FOnWin OnWin;

	//player 0 = index 0 etc
	UPROPERTY(Replicated,BlueprintReadOnly)
		TArray<int32> players_scores;

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerAddScore(int32 player, int32 score);
	void ServerAddScore_Implementation(int32 player, int32 score);
	bool ServerAddScore_Validate(int32 player, int32 score) { return true; };
	
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty > & OutLifetimeProps) const;
	
	UPROPERTY(Replicated, BlueprintReadOnly)
		bool bPostWin;

	UPROPERTY(Replicated, BlueprintReadOnly)
		int32 WinPlayer;

	void OnWinTimer();
private:
	FTimerHandle WinTimer;
};
