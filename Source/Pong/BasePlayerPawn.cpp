// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "BasePlayerPawn.h"
#include "PaperSpriteComponent.h"
#include "PongAIController.h"

// Sets default values
ABasePlayerPawn::ABasePlayerPawn() {
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->AttachTo(GetRootComponent());
	Camera->SetAbsolute(true, true, true);
	Camera->SetWorldLocation(FVector(0, 0, 1000));
	Camera->SetProjectionMode(ECameraProjectionMode::Orthographic);
	Camera->SetOrthoWidth(1000);
	Camera->SetWorldRotation(FRotator(-90, 0, 0));
	AIControllerClass = APongAIController::StaticClass();
	ReplicatedMovement.LocationQuantizationLevel = EVectorQuantization::RoundTwoDecimals;
	ReplicatedMovement.VelocityQuantizationLevel = EVectorQuantization::RoundTwoDecimals;
}

// Called when the game starts or when spawned
void ABasePlayerPawn::BeginPlay() {
	Super::BeginPlay();

}

// Called every frame
void ABasePlayerPawn::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABasePlayerPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent) {
	Super::SetupPlayerInputComponent(InputComponent);

}

void ABasePlayerPawn::PlayerMove(float a) {
}