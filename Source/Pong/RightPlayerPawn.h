// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BasePlayerPawn.h"
#include "RightPlayerPawn.generated.h"

UCLASS()
class PONG_API ARightPlayerPawn : public ABasePlayerPawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ARightPlayerPawn();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	
	void PlayerMove(float a);

	void LeftPlayerMove(float a);

};
